#!/usr/bin/python3
# Script to build a docker image based on installed nvidia drivers

import os
import argparse


def parse_arguments():
    ''' Parse command line arguments '''
    parser = argparse.ArgumentParser(description="Builds the nuav docker image")

    # parser.add_argument('-d', "--debug", dest="debug", action="store_true", default=False, help="Enable debug")

    return parser.parse_args()


def main(args):
    # Get nuav_ws location
    nuav_ws_location = os.path.join(os.getcwd(), '..')

    # Look for an nvidia driver
    if not os.path.exists("/proc/driver/nvidia/version"):
        response = input("NVIDIA driver not found, continue without? (y/N)")
        
        if not "y" == response.lower():
            exit(0)

        print("Building without drivers...")
    else:
        # Build with drivers
        print("Building with drivers...")
        with open("/proc/driver/nvidia/version") as driver_file:
            driver_version = driver_file.read().split(' ')[8] # Gets the 9th "word" which is the driver version
        print(f"NVIDIA driver version: {driver_version}")

    os.system(f"sudo docker build -t nuav_img -f {nuav_ws_location}/scripts/docker/Dockerfile --build-arg DRIVER_VERSION={driver_version} {nuav_ws_location}")


if __name__ == "__main__":
    args = parse_args()
    main(args)
    