#!/usr/bin/python3
#
# NUAV script to run the nuav docker image
# based off of https://github.com/PX4/PX4-Autopilot/blob/master/Tools/docker_run.sh
#

import argparse
import os

def parse_args():
    ''' Parse command line arguments '''
    parser = argparse.ArgumentParser(description="Builds all neccesary code. Only builds ROS by default")

    parser.add_argument('-i', "--docker-img", dest="docker_img", default="nuav_img:latest", help="The image to run")
    parser.add_argument('-b', "--bash", dest="use_bash", action="store_true", default=False, help="Dissables terminator and runs in a bash terminal instead")

    return parser.parse_args()

def main(args):
    # Get nuav_ws location
    nuav_ws_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    # create cache for cmake that persists on the host
    ccache_dir = os.path.expanduser("~/.ccache")
    if not os.path.exists(ccache_dir):
        os.mkdir(ccache_dir)

    # Run X server on host machine for GUI
    if os.system("command -v xhost >/dev/null 2>&1") != 0:
        print("Running without X server.")
    else:
        os.system("xhost +local:root")
    
    # run the image, removing other coppies if they exist
    # to run as root I removed:  --env=LOCAL_USER_ID="$(id -u)" 
    # "privlidged" allows USB acces, "network=host" is for unrestricted network access
    docker_cmd = f'''docker run -it --rm --privileged --network=host \
        -w "/nuav_ws" \
        --env=CCACHE_DIR="{ccache_dir}" \
        --volume={ccache_dir}:{ccache_dir}:rw \
        --volume={nuav_ws_location}:"/nuav_ws":rw \
        --env="DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
        --name=nuav-container \
        {args.docker_img} {"bash" if args.use_bash else "terminator --title=NUAVDocker"}
    '''

    os.system(docker_cmd)

if __name__ == "__main__":
    args = parse_args()
    main(args)
