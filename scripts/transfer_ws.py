#!/usr/bin/python3

# Author: Rohit Josyula
# The purpose of this script is to transfer the NUAV workspace while specifying which directories
# should not be transferred. The user should specify the username of the host they are transferring to
# when running this script and the files will be transferred to it


# Import necessary libraries to do file transfer via SCP and SSH and pass in arguments

import argparse
import paramiko
import os
from scp import SCPClient

# parser object created to take arguments as inputs, followed by args object to access them. 
# Argument that needs to be specified is what the username of the remote host is
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--username', type=str, required=True)
args = parser.parse_args()

# client is created given inputs identifying it
def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client
           
# dictionary to store possible nodes to transfer to. Update it when more nodes are used
thisdict = {
    "splinter": '192.168.0.178'
}
port = 22

# check to make sure given username is contained in the dictionary, else it will display 
# possible usernames to choose from
if args.username not in thisdict:
    print(f"Error: please type in a valid username \n Ex: {list(thisdict.keys())}")
    exit()

ssh = createSSHClient(thisdict[args.username], port, args.username, 'nuav')
scp = SCPClient(ssh.get_transport()) # create scp object from ssh client
currentWorkingDirectory = (os.getcwd().split("/scripts"))[0] # obtain current working directory and parse it to get path to workspace
remotePath = "~/nuav_ws/" # name of the directory to be created on remote host. 

# list of directories that should not be transferred. Update it as needed
excluded_directories = ["build", "install", "log", ".git", "src/nuav_gazebo", "src/PX4-Autopilot"]

# dictionary that will contain nested dictionaries to store a tree hierarchy of excluded directories
organized_directories = {}

# function to create a tree hierarchy of the directories to be excluded
def make_organized_directories(currentDirectory, splittedList, levels, currentLevel=0):

        # if the first level of the directory is not in the first set of keys, add it as a key and its value to be another dictionary
        # Then recursively call the function by examining the next level of the directory
        if splittedList[0] not in organized_directories.keys():
            organized_directories[splittedList[0]] = {} 
            currentLevel = currentLevel + 1
            make_organized_directories(organized_directories, splittedList, levels, currentLevel)
        # if the first level of the directory already exists as a key, look at the next level. This case is added because 
        # in order to advance to the next condition, currentLevel>=1, else there is an IndexOutOfBounds exception
        elif currentLevel==0:
            currentLevel = currentLevel + 1
            make_organized_directories(organized_directories, splittedList, levels, currentLevel)
        # if the current level matches total levels, this means the entire directory has been iterated through and we can exit the function
        elif currentLevel==levels:
            pass
        # if the current level of the directory is not in the list of keys of the current dictionary, which is just the value
        # for the previous level of the directory stored as a key, add it as a key and set its value to be another dictionary
        elif splittedList[currentLevel] not in currentDirectory.get(splittedList[currentLevel-1]).keys():
            currentDirectory[splittedList[currentLevel-1]][splittedList[currentLevel]] = {}
            currentDirectory = currentDirectory.get(splittedList[currentLevel-1])
            currentLevel = currentLevel + 1
            make_organized_directories(currentDirectory, splittedList, levels, currentLevel)
        # if the current level of the directory is already a key of the current dictionary, move onto the next level
        else:
            nextDirectory = currentDirectory.get(splittedList[currentLevel-1])
            currentLevel = currentLevel + 1
            make_organized_directories(nextDirectory, splittedList, levels, currentLevel)


# function to iterate through the NUAV workspace and check with the dictionary tree hierarchy
# of whether the current directory can be transferred over
def directories_to_transfer(currentDirectory, currentDictionary):
    # iterate through all the content in the current directory
    for dir in os.listdir(currentDirectory):
        # if current directory is not in the excluded directories list, get the full local path to this directory
        # and place it on the created remote path in the remote host
        if dir not in currentDictionary.keys():
            nextDirectory = currentDirectory + "/" + dir
            getRemotePath = nextDirectory.split("/nuav_ws/")
            createdRemotePath = remotePath + getRemotePath[1]
            print("Adding " + nextDirectory + " to " + createdRemotePath + " in " + args.username)
            scp.put(nextDirectory, createdRemotePath, recursive=True)    
        # if current directory is in excluded directories list and does not have sublevels, move onto the next directory    
        elif currentDictionary.get(dir) == {}:
            pass
        # if current directory is in excluded directories list and has sublevels, move onto the next level and the next dictionary
        else:
            nextDirectory = currentDirectory + "/" + dir
            getDirectory = nextDirectory.split("/nuav_ws/")
            createDirectory = remotePath + getDirectory[1]
            ssh.exec_command('mkdir ' + createDirectory)
            nextDictionary = currentDictionary.get(dir)
            directories_to_transfer(nextDirectory, nextDictionary)


# iterate through each directory in the excluded directories list, split it into its sublevels,
# get the total number of sublevels, and pass into function that will create tree hierarchy
for d in excluded_directories:
    splittedList = d.split("/")
    levels = len(splittedList)
    make_organized_directories(organized_directories, splittedList, levels)

# create the NUAV workspace directory in the remote host
ssh.exec_command('mkdir ' + remotePath)

# transfer the files of the NUAV workspace to the remote host
print ("Beginning transfer now")
directories_to_transfer(currentWorkingDirectory, organized_directories)

# inform the user that workspace has transferred and also which directories did not get transferred in case there is a problem
print ("Transfer complete\n")
print ("Directories not transferred: ")
print(excluded_directories)
  
