
::
:: Script that simply builds the docker image
::
@echo off&setlocal

for %%i in ("%~dp0..") do set "nuav_ws_path=%%~fi"
echo using nuav_ws: %nuav_ws_path%

:: Build the drockerfile from nuav_ws
docker build -t nuav_img -f "%nuav_ws_path%\scripts\docker\Dockerfile" "%nuav_ws_path%"
