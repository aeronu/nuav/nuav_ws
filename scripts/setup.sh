#!/bin/bash
# Sets up terminal environment to recognize our code
# Also registers some custom commands (aliases)

NUAV_WS=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )

# Source ROS and our code if it exists
source "/opt/ros/${ROS_DISTRO}/setup.bash" 
if [ -f "$NUAV_WS/install/setup.bash" ]; then source "$NUAV_WS/install/setup.bash"; fi


# Tell Gazebo where all our custom models/worlds are
source /usr/share/gazebo/setup.sh
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:$NUAV_WS/src/nuav_gazebo/nuav_gazebo/models
export GAZEBO_RESOURCE_PATH=${GAZEBO_RESOURCE_PATH}:$NUAV_WS/src/nuav_gazebo/nuav_gazebo/worlds
source $NUAV_WS/src/PX4-Autopilot/Tools/setup_gazebo.bash $NUAV_WS/src/PX4-Autopilot $NUAV_WS/src/PX4-Autopilot/build/px4_sitl_default > /dev/null


# Make ROS output pretty colors
export RCUTILS_COLORIZED_OUTPUT=1
export RCUTILS_CONSOLE_OUTPUT_FORMAT="[{severity}][{function_name}():{line_number}]: {message}"


### Aliases (custom commands)
alias sim="ros2 launch nuav_mission start_single_sim.launch.py"
alias killros2='killall -9 gzserver gzclient _ros2_daemon px4 ros2'
