:: NUAV script to run the nuav docker image
:: It also configures Gazebo/Airsim 
::


@echo off&setlocal

:: Check if we are running gazebo or airsim
if /I "%~1"=="airsim" (
    echo Using AirSim

    :: fun thing to find the WSL adapter IP
    for /f "tokens=3 delims=: " %%i  in ('netsh interface ip show config name^="vEthernet (WSL)" ^| findstr "IP Address" ^| findstr [0-9]') do set SIM_IP=%%i 
) else (
    echo Using Gazebo/VcXsrv

    :: start VcXsrv if it isnt already running (no idea how this works but it does)
    tasklist /nh /fi "imagename eq VcXsrv.exe" | find /i "VcXsrv.exe" > nul || (start /d "C:\Program Files\VcXsrv" vcxsrv.exe :0 -ac -terminate -lesspointer -multiwindow -clipboard -nowgl -dpi auto)
)

:: Docker image to run
:: set DOCKER_IMG="px4io/px4-dev-ros2-foxy:2021-07-13"
set DOCKER_IMG="nuav_img:latest"

:: find nuav_ws 
for %%i in ("%~dp0..") do set "nuav_ws_path=%%~fi"
echo using nuav_ws: %nuav_ws_path%

:: create cache for cmake that persists on the host
set CCACHE_DIR=%nuav_ws_path%\.ccache
if not exist "%CCACHE_DIR%" mkdir "%CCACHE_DIR%"

:: run the image, removing other coppies if they exist
docker run --rm --privileged  --network=host^
    -w "/nuav_ws" ^
    --env="CCACHE_DIR=/.ccache" ^
    --volume="%CCACHE_DIR%":"/.ccache":rw ^
    --volume="%nuav_ws_path%":"/nuav_ws":rw ^
    --env="DISPLAY=host.docker.internal:0.0" ^
    --name=nuav-container ^
    %DOCKER_IMG% terminator --title=NUAVDocker
